package com.nycschools.network;


import com.nycschools.model.School;
import com.nycschools.model.Score;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {
    @GET("s3k6-pzi2.json")
    Single<List<School>> getSchoolList();

    @GET("f9bf-2cp4.json")
    Single<List<Score>> getScores(@Query("dbn") String dbn);

}
