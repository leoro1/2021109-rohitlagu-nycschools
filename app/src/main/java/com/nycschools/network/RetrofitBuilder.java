package com.nycschools.network;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilder {
    private static RetrofitBuilder instance;

    private final Api apiBuilder;

    private RetrofitBuilder()
    {
        apiBuilder =  new Retrofit.Builder()
                .baseUrl(Url.baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Api.class);
    }

    public static RetrofitBuilder getInstance()
    {
        if(instance == null){
            instance = new RetrofitBuilder();
        }
        return instance;
    }
    public Api api() {
        return apiBuilder;
    }
}

