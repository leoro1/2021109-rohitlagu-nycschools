package com.nycschools.network;

public class State {
    public static final int ERROR = -1;
    public static final int LOADING = 0;
    public static final int SUCCESS = 1;
}
