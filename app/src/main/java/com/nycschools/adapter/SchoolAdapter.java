package com.nycschools.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nycschools.R;
import com.nycschools.model.School;

import java.util.List;

public class SchoolAdapter extends RecyclerView.Adapter<SchoolViewHolder> {

    public interface ItemClickListener {
        void itemClicked(String id, String name);
    }

   private final ItemClickListener itemClickListener;

    private List<School> schools;

    public  SchoolAdapter(@NonNull List<School> schools,ItemClickListener itemClickListener) {
        this.schools = schools;
        this.itemClickListener = itemClickListener;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setSchools(List<School> schools) {
        this.schools = schools;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.school_row, parent, false);
        return new SchoolViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolViewHolder holder, int position) {
        holder.bind(schools.get(position), v -> {
            if (itemClickListener != null && schools.get(position) != null) {
                schools.get(position).getDbn();
                itemClickListener.itemClicked(schools.get(position).getDbn(), schools.get(position).getSchool_name());
            }
        });
    }

    @Override
    public int getItemCount() {
        return schools.size();
    }
}



