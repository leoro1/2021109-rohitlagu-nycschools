package com.nycschools;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.nycschools.databinding.FragmentSecondBinding;
import com.nycschools.network.State;
import com.nycschools.viewModel.ScoreViewModel;

import java.util.Objects;


public class SecondFragment extends Fragment {

    private FragmentSecondBinding binding;
    private ScoreViewModel scoreViewModel;

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        binding = FragmentSecondBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        assert getArguments() != null;
        String id = getArguments().getString("schoolID");
        scoreViewModel = new
                ViewModelProvider(this).get(ScoreViewModel.class);
        scoreViewModel.fetch(id);
        scoreViewModel.viewState.observe(getViewLifecycleOwner(), state -> {
            if (state == State.ERROR) {
                showErrorModal();
            }
        });
        scoreViewModel.schoolScoreMutableLiveData.observe(getViewLifecycleOwner(), schoolStatistics -> setDisplayData()
        );

    }

    private void setDisplayData() {
        binding.schoolName.setText(Objects.requireNonNull(scoreViewModel.schoolScoreMutableLiveData.getValue()).getSchoolName());
        binding.satMathValueTextView.setText(scoreViewModel.schoolScoreMutableLiveData.getValue().getSatMathAvgScore());
        binding.satReadingValueTextView.setText(scoreViewModel.schoolScoreMutableLiveData.getValue().getSatCriticalReadingAvgScore());
        binding.satWritingValueTextView.setText(scoreViewModel.schoolScoreMutableLiveData.getValue().getSatWritingAvgScore());
        binding.satValueTextView.setText(scoreViewModel.schoolScoreMutableLiveData.getValue().getNumOfSatTestTakers());
    }

    private void showErrorModal() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setMessage("No School score data available")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        getActivity().onBackPressed();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}