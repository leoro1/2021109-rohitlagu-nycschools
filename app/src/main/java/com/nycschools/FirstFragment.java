package com.nycschools;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nycschools.adapter.SchoolAdapter;
import com.nycschools.databinding.FragmentFirstBinding;
import com.nycschools.model.School;
import com.nycschools.viewModel.SchoolViewModel;
import java.util.ArrayList;

public class FirstFragment extends Fragment {

    private FragmentFirstBinding binding;
    private SchoolAdapter schoolsAdapter;


    private final SchoolAdapter.ItemClickListener itemClickListener = (id, name) -> {
        Bundle bundle = new Bundle();
        bundle.putString("schoolID", id);
        NavHostFragment.findNavController(FirstFragment.this)
                .navigate(R.id.action_FirstFragment_to_SecondFragment,bundle);
    };

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        binding = FragmentFirstBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        schoolsAdapter = new SchoolAdapter(new ArrayList<>(),itemClickListener);
        binding.schoolListRecyclerView.setHasFixedSize(true);
        binding.schoolListRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.schoolListRecyclerView.setAdapter(schoolsAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        SchoolViewModel mSchoolViewModel = new
                ViewModelProvider(requireActivity()).get(SchoolViewModel.class);
        mSchoolViewModel.getSchoolList();

        mSchoolViewModel.schoolsMutableLiveData.observe(getViewLifecycleOwner(), schools -> {
            if (schools != null && schools.size() > 0) {
                schoolsAdapter.setSchools((ArrayList<School>) schools);
            }
        });
    }

    @Override
    public void onDestroyView () {
        super.onDestroyView();
        binding = null;
    }
}