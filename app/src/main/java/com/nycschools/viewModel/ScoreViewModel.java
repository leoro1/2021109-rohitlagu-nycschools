package com.nycschools.viewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.nycschools.model.Score;
import com.nycschools.repository.ApiRepository;

public class ScoreViewModel extends ViewModel {

    public MutableLiveData<Score> schoolScoreMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<Integer> viewState = new MutableLiveData<>(0);

    public void fetch(String schoolId)
    {
        ApiRepository.getInstance().fetchSchoolScoreDetails(schoolId, schoolScoreMutableLiveData, viewState);
    }
}